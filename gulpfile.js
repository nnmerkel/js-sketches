const gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    bs = require('browser-sync').create();

    sassOpts = {
        outputStyle: 'compressed', //or expanded
    };

function browserSync() {
    bs.init({
        //watch: true,
        server: 'src'
    });
}

function browserSyncReload() {
    bs.reload();
}

function css() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass(sassOpts))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('src/css'))
        .pipe(bs.reload({ stream: true }));
}

function watchFiles() {
    gulp.watch('src/scss/*.scss').on('change', gulp.series(css, browserSyncReload));
    gulp.watch('src/index.html').on('change', browserSyncReload);
    gulp.watch('src/js/*.js').on('change', browserSyncReload);
}

function watch(done) {
    gulp.parallel(watchFiles, browserSync)(done);
}

exports.css = css;
exports.watch = watch;
exports.default = watch;
